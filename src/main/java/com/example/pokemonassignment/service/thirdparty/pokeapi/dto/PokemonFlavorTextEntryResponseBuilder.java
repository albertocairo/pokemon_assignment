package com.example.pokemonassignment.service.thirdparty.pokeapi.dto;

public class PokemonFlavorTextEntryResponseBuilder {
    private String flavorText;
    private PokemonFlavorTextEntryLanguageResponse language;

    public PokemonFlavorTextEntryResponseBuilder withFlavorText(String flavorText) {
        this.flavorText = flavorText;
        return this;
    }

    public PokemonFlavorTextEntryResponseBuilder withLanguage(PokemonFlavorTextEntryLanguageResponse language) {
        this.language = language;
        return this;
    }

    public PokemonFlavorTextEntryResponse build() {
        return new PokemonFlavorTextEntryResponse(flavorText, language);
    }
}

package com.example.pokemonassignment.model;

public class Pokemon {
    private String name;
    private String description;
    private String habitat;
    private Boolean legendary;

    public Pokemon(String name, String description, String habitat, Boolean legendary) {
        this.name = name;
        this.description = description;
        this.habitat = habitat;
        this.legendary = legendary;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getHabitat() {
        return habitat;
    }

    public Boolean getLegendary() {
        return legendary;
    }
}

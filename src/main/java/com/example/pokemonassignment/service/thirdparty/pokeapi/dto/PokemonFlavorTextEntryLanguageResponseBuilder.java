package com.example.pokemonassignment.service.thirdparty.pokeapi.dto;

public class PokemonFlavorTextEntryLanguageResponseBuilder {
    private String name;

    public PokemonFlavorTextEntryLanguageResponseBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PokemonFlavorTextEntryLanguageResponse build() {
        return new PokemonFlavorTextEntryLanguageResponse(name);
    }
}

package com.example.pokemonassignment.service.pokemon;

import com.example.pokemonassignment.exception.PokemonNotFoundException;
import com.example.pokemonassignment.model.Pokemon;
import com.example.pokemonassignment.model.PokemonBuilder;
import com.example.pokemonassignment.service.pokemon.converter.FunTranslationsResponseConverter;
import com.example.pokemonassignment.service.pokemon.converter.PokemonSpeciesResponseConverter;
import com.example.pokemonassignment.service.thirdparty.funtranslations.FunTranslationsService;
import com.example.pokemonassignment.service.thirdparty.funtranslations.enums.TranslationType;
import com.example.pokemonassignment.service.thirdparty.pokeapi.PokeApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PokemonService {
    private static final Logger logger = LoggerFactory.getLogger(PokemonService.class);

    private PokeApiService pokeapiService;
    private FunTranslationsService funTranslationsService;
    private PokemonSpeciesResponseConverter pokemonSpeciesResponseConverter;
    private FunTranslationsResponseConverter funTranslationsResponseConverter;

    public PokemonService(PokeApiService pokeapiService, FunTranslationsService funTranslationsService, PokemonSpeciesResponseConverter pokemonSpeciesResponseConverter, FunTranslationsResponseConverter funTranslationsResponseConverter) {
        this.pokeapiService = pokeapiService;
        this.funTranslationsService = funTranslationsService;
        this.pokemonSpeciesResponseConverter = pokemonSpeciesResponseConverter;
        this.funTranslationsResponseConverter = funTranslationsResponseConverter;
    }

    public Pokemon getPokemonByName(String name) {
        return pokeapiService.getPokemonSpeciesByName(name)
                .map(pokemonSpeciesResponseConverter::convert)
                .orElseThrow(() -> new PokemonNotFoundException("Pokemon not found by name: [" + name + "]"));
    }

    public Pokemon getPokemonTranslatedByName(String name) {
        Pokemon pokemon = getPokemonByName(name);

        TranslationType translationType = pokemon.getLegendary() || "cave".equalsIgnoreCase(pokemon.getHabitat())
                ? TranslationType.YODA
                : TranslationType.SHAKESPEARE;

        String description = funTranslationsService.getTranslation(pokemon.getDescription(), translationType)
                .map(funTranslationsResponseConverter::convert)
                .orElseGet(() -> {
                    logger.info("Could not retrieve translation: falling back to standard description");
                    return pokemon.getDescription();
                });

        return PokemonBuilder.from(pokemon)
                .withDescription(description)
                .build();
    }
}

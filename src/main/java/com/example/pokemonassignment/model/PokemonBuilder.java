package com.example.pokemonassignment.model;

public class PokemonBuilder {
    private String name;
    private String description;
    private String habitat;
    private Boolean legendary;

    public PokemonBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PokemonBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public PokemonBuilder withHabitat(String habitat) {
        this.habitat = habitat;
        return this;
    }

    public PokemonBuilder withLegendary(Boolean legendary) {
        this.legendary = legendary;
        return this;
    }

    public Pokemon build() {
        return new Pokemon(name, description, habitat, legendary);
    }

    public static PokemonBuilder from(Pokemon pokemon) {
        return new PokemonBuilder()
                .withName(pokemon.getName())
                .withDescription(pokemon.getDescription())
                .withHabitat(pokemon.getHabitat())
                .withLegendary(pokemon.getLegendary());
    }
}

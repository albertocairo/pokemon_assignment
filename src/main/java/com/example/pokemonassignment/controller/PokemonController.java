package com.example.pokemonassignment.controller;

import com.example.pokemonassignment.model.Pokemon;
import com.example.pokemonassignment.service.pokemon.PokemonService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PokemonController {
    private PokemonService pokemonService;

    public PokemonController(PokemonService pokemonService) {
        this.pokemonService = pokemonService;
    }

    @GetMapping("/pokemon/{name}")
    public Pokemon getPokemonByName(@PathVariable("name") String name) {
        return pokemonService.getPokemonByName(name);
    }

    @GetMapping("/pokemon/translated/{name}")
    public Pokemon getPokemonTranslatedByName(@PathVariable("name") String name) {
        return pokemonService.getPokemonTranslatedByName(name);
    }
}

package com.example.pokemonassignment.service.thirdparty.funtranslations.dto;

public class FunTranslationsResponseBuilder {
    private FunTranslationsResponseContents contents;

    public FunTranslationsResponseBuilder withContents(FunTranslationsResponseContents contents) {
        this.contents = contents;
        return this;
    }

    public FunTranslationsResponse build() {
        return new FunTranslationsResponse(contents);
    }
}

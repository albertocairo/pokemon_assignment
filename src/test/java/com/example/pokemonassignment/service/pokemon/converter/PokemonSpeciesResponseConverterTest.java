package com.example.pokemonassignment.service.pokemon.converter;

import com.example.pokemonassignment.model.Pokemon;
import com.example.pokemonassignment.service.thirdparty.pokeapi.dto.*;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

class PokemonSpeciesResponseConverterTest {

    @Test
    void shouldConvert() {
        // Given
        PokemonSpeciesResponseConverter sut = new PokemonSpeciesResponseConverter();

        String expectedDescription = "Lorem ipsum";
        String expectedHabitat = "Habitat";
        PokemonSpeciesResponse source = new PokemonSpeciesResponseBuilder()
                .withName("Name")
                .withFlavorTextEntries(
                        Arrays.asList(
                                new PokemonFlavorTextEntryResponseBuilder()
                                        .withFlavorText("dolor sit amet")
                                        .withLanguage(
                                                new PokemonFlavorTextEntryLanguageResponseBuilder()
                                                        .withName("fr")
                                                        .build())
                                        .build(),
                                new PokemonFlavorTextEntryResponseBuilder()
                                        .withFlavorText(expectedDescription)
                                        .withLanguage(
                                                new PokemonFlavorTextEntryLanguageResponseBuilder()
                                                        .withName("en")
                                                        .build())
                                        .build()))
                .withHabitat(
                        new PokemonHabitatResponseBuilder()
                                .withName(expectedHabitat)
                                .build())
                .withLegendary(true)
                .build();

        // When
        Pokemon target = sut.convert(source);

        // Then
        assertThat(target)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .matches(t -> t.getDescription().equals(expectedDescription))
                .matches(t -> t.getHabitat().equals(expectedHabitat))
                .usingRecursiveComparison()
                .ignoringFields("description", "habitat")
                .isEqualTo(source);
    }
}
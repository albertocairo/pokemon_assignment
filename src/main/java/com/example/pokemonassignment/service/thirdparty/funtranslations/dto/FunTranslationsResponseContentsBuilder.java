package com.example.pokemonassignment.service.thirdparty.funtranslations.dto;

public class FunTranslationsResponseContentsBuilder {
    private String translated;

    public FunTranslationsResponseContentsBuilder withTranslated(String translated) {
        this.translated = translated;
        return this;
    }

    public FunTranslationsResponseContents build() {
        return new FunTranslationsResponseContents(translated);
    }
}

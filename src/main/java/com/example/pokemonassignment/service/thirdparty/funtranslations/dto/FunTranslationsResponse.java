package com.example.pokemonassignment.service.thirdparty.funtranslations.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FunTranslationsResponse {
    private FunTranslationsResponseContents contents;

    @JsonCreator
    public FunTranslationsResponse(@JsonProperty("contents") FunTranslationsResponseContents contents) {
        this.contents = contents;
    }

    public FunTranslationsResponseContents getContents() {
        return contents;
    }
}

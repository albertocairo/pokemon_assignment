# build stage
FROM maven AS build

WORKDIR /usr/src/pokemon_assignment

COPY pom.xml .
RUN mvn -B dependency:go-offline

COPY . .
RUN mvn package

# final stage
FROM openjdk

WORKDIR /app

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app/pokemon_assignment-0.0.1-SNAPSHOT.jar"]

COPY --from=build /usr/src/pokemon_assignment/target/pokemon_assignment-0.0.1-SNAPSHOT.jar .


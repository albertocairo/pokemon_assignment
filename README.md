## Pokemon Assignment

### Instructions

To build the project:
```
docker image build -t pokemon_assignment .
```

To run the project:
```
docker container run -p 8080:8080 pokemon_assignment
```

Examples:
```
http://localhost:8080/pokemon/mewtwo
http://localhost:8080/pokemon/translated/mewtwo
```
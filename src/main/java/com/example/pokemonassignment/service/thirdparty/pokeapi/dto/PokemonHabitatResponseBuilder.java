package com.example.pokemonassignment.service.thirdparty.pokeapi.dto;

public class PokemonHabitatResponseBuilder {
    private String name;

    public PokemonHabitatResponseBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PokemonHabitatResponse build() {
        return new PokemonHabitatResponse(name);
    }
}

package com.example.pokemonassignment.service.pokemon.converter;

import com.example.pokemonassignment.service.thirdparty.funtranslations.dto.FunTranslationsResponse;
import com.example.pokemonassignment.service.thirdparty.funtranslations.dto.FunTranslationsResponseContents;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

@Component
public class FunTranslationsResponseConverter implements Converter<FunTranslationsResponse, String> {

    @Override
    public String convert(FunTranslationsResponse source) {
        Objects.requireNonNull(source);

        return Optional.ofNullable(source)
                .map(FunTranslationsResponse::getContents)
                .map(FunTranslationsResponseContents::getTranslated)
                .orElse(null);
    }
}

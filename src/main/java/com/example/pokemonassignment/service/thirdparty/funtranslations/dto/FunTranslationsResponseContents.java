package com.example.pokemonassignment.service.thirdparty.funtranslations.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FunTranslationsResponseContents {
    private String translated;

    @JsonCreator
    public FunTranslationsResponseContents(@JsonProperty("translated") String translated) {
        this.translated = translated;
    }

    public String getTranslated() {
        return translated;
    }
}

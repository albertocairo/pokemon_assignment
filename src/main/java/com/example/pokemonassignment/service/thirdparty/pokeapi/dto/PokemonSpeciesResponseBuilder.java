package com.example.pokemonassignment.service.thirdparty.pokeapi.dto;

import java.util.List;

public class PokemonSpeciesResponseBuilder {
    private String name;
    private List<PokemonFlavorTextEntryResponse> flavorTextEntries;
    private PokemonHabitatResponse habitat;
    private Boolean legendary;

    public PokemonSpeciesResponseBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PokemonSpeciesResponseBuilder withFlavorTextEntries(List<PokemonFlavorTextEntryResponse> flavorTextEntries) {
        this.flavorTextEntries = flavorTextEntries;
        return this;
    }

    public PokemonSpeciesResponseBuilder withHabitat(PokemonHabitatResponse habitat) {
        this.habitat = habitat;
        return this;
    }

    public PokemonSpeciesResponseBuilder withLegendary(Boolean legendary) {
        this.legendary = legendary;
        return this;
    }

    public PokemonSpeciesResponse build() {
        return new PokemonSpeciesResponse(name, flavorTextEntries, habitat, legendary);
    }
}

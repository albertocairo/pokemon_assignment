package com.example.pokemonassignment.service.thirdparty.funtranslations;

import com.example.pokemonassignment.service.thirdparty.funtranslations.dto.FunTranslationsResponse;
import com.example.pokemonassignment.service.thirdparty.funtranslations.dto.FunTranslationsResponseBuilder;
import com.example.pokemonassignment.service.thirdparty.funtranslations.enums.TranslationType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FunTranslationsServiceTest {
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private FunTranslationsService sut;

    @AfterEach
    void afterEachTest() {
        verifyNoMoreInteractions(restTemplate);
    }

    @Test
    void shouldGetYodaTranslation() {
        // Given
        String text = "Lorem ipsum";
        TranslationType translationType = TranslationType.YODA;

        FunTranslationsResponse expected = new FunTranslationsResponseBuilder().build();
        ResponseEntity<FunTranslationsResponse> response = ResponseEntity
                .status(HttpStatus.OK)
                .body(expected);
        when(restTemplate.exchange(any(URI.class), eq(HttpMethod.GET), isNull(), eq(FunTranslationsResponse.class)))
                .thenReturn(response);

        // When
        Optional<FunTranslationsResponse> actual = sut.getTranslation(text, translationType);

        // Then
        assertThat(actual)
                .isPresent()
                .get()
                .isEqualTo(expected);
        ArgumentCaptor<URI> captor = ArgumentCaptor.forClass(URI.class);
        verify(restTemplate).exchange(captor.capture(), eq(HttpMethod.GET), isNull(), eq(FunTranslationsResponse.class));
        assertThat(captor.getValue())
                .asString()
                .contains("https://api.funtranslations.com/translate/yoda.json?text=");
    }

    @Test
    void shouldGetShakespeareTranslation() {
        // Given
        String text = "Lorem ipsum";
        TranslationType translationType = TranslationType.SHAKESPEARE;

        FunTranslationsResponse expected = new FunTranslationsResponseBuilder().build();
        ResponseEntity<FunTranslationsResponse> response = ResponseEntity
                .status(HttpStatus.OK)
                .body(expected);
        when(restTemplate.exchange(any(URI.class), eq(HttpMethod.GET), isNull(), eq(FunTranslationsResponse.class)))
                .thenReturn(response);

        // When
        Optional<FunTranslationsResponse> actual = sut.getTranslation(text, translationType);

        // Then
        assertThat(actual)
                .isPresent()
                .get()
                .isEqualTo(expected);
        ArgumentCaptor<URI> captor = ArgumentCaptor.forClass(URI.class);
        verify(restTemplate).exchange(captor.capture(), eq(HttpMethod.GET), isNull(), eq(FunTranslationsResponse.class));
        assertThat(captor.getValue())
                .asString()
                .contains("https://api.funtranslations.com/translate/shakespeare.json?text=");
    }

    @ParameterizedTest
    @EnumSource(value = TranslationType.class)
    void shouldGetTranslation_exchangeThrowsException(TranslationType translationType) {
        // Given
        String text = "Lorem ipsum";

        when(restTemplate.exchange(any(URI.class), eq(HttpMethod.GET), isNull(), eq(FunTranslationsResponse.class)))
                .thenThrow(RuntimeException.class);

        // When
        Optional<FunTranslationsResponse> actual = sut.getTranslation(text, translationType);

        // Then
        assertThat(actual).isEmpty();
        verify(restTemplate).exchange(any(URI.class), eq(HttpMethod.GET), isNull(), eq(FunTranslationsResponse.class));
    }

    @ParameterizedTest
    @EnumSource(value = TranslationType.class)
    void shouldGetTranslation_responseStatusCodeIsNotCreated(TranslationType translationType) {
        // Given
        String text = "Lorem ipsum";
        FunTranslationsResponse expected = new FunTranslationsResponseBuilder().build();
        ResponseEntity<FunTranslationsResponse> response = ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(expected);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), isNull(), eq(FunTranslationsResponse.class), anyMap()))
                .thenReturn(response);

        // When
        Optional<FunTranslationsResponse> actual = sut.getTranslation(text, translationType);

        // Then
        assertThat(actual).isEmpty();
        verify(restTemplate).exchange(any(URI.class), eq(HttpMethod.GET), isNull(), eq(FunTranslationsResponse.class));
    }
}
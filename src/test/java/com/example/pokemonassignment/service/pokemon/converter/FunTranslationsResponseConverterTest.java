package com.example.pokemonassignment.service.pokemon.converter;

import com.example.pokemonassignment.service.thirdparty.funtranslations.dto.FunTranslationsResponse;
import com.example.pokemonassignment.service.thirdparty.funtranslations.dto.FunTranslationsResponseBuilder;
import com.example.pokemonassignment.service.thirdparty.funtranslations.dto.FunTranslationsResponseContentsBuilder;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class FunTranslationsResponseConverterTest {

    @Test
    void shouldConvert() {
        // Given
        FunTranslationsResponseConverter sut = new FunTranslationsResponseConverter();

        String expected = "Lorem ipsum";
        FunTranslationsResponse source = new FunTranslationsResponseBuilder()
                .withContents(
                        new FunTranslationsResponseContentsBuilder()
                                .withTranslated(expected)
                                .build())
                .build();

        // When
        String actual = sut.convert(source);

        // Then
        assertThat(actual).isEqualTo(expected);
    }
}
package com.example.pokemonassignment.service.thirdparty.funtranslations;

import com.example.pokemonassignment.service.thirdparty.funtranslations.dto.FunTranslationsResponse;
import com.example.pokemonassignment.service.thirdparty.funtranslations.enums.TranslationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;
import java.util.Optional;

@Service
public class FunTranslationsService {
    private static final Logger logger = LoggerFactory.getLogger(FunTranslationsService.class);

    private RestTemplate restTemplate;

    public FunTranslationsService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private static final String YODA_URI = "https://api.funtranslations.com/translate/yoda.json?text={text}";
    private static final String SHAKESPEARE_URI = "https://api.funtranslations.com/translate/shakespeare.json?text={text}";

    public Optional<FunTranslationsResponse> getTranslation(String text, TranslationType translationType) {
        Objects.requireNonNull(text);
        Objects.requireNonNull(translationType);

        // It seems the API doesn't like line terminators, so we replace every occurrence with a space.
        text = text.replaceAll("\\R+", " ");

        String uri = translationType == TranslationType.YODA
                ? YODA_URI
                : SHAKESPEARE_URI;

        ResponseEntity<FunTranslationsResponse> response;
        try {
            response = restTemplate.exchange(
                UriComponentsBuilder.fromUriString(uri).build(text),
                HttpMethod.GET,
                null,
                FunTranslationsResponse.class);
        } catch (Exception e) {
            logger.info("Error calling funtranslations: ", e);
            return Optional.empty();
        }

        return Optional.of(response)
                .map(ResponseEntity::getStatusCode)
                .filter(HttpStatus.OK::equals)
                .map(__ -> response.getBody());
    }
}

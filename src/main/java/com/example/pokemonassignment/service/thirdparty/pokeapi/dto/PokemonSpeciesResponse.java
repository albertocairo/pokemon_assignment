package com.example.pokemonassignment.service.thirdparty.pokeapi.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PokemonSpeciesResponse {
    private String name;
    private List<PokemonFlavorTextEntryResponse> flavorTextEntries;
    private PokemonHabitatResponse habitat;
    private Boolean legendary;

    @JsonCreator
    public PokemonSpeciesResponse(
            @JsonProperty("name") String name,
            @JsonProperty("flavor_text_entries") List<PokemonFlavorTextEntryResponse> flavorTextEntries,
            @JsonProperty("habitat") PokemonHabitatResponse habitat,
            @JsonProperty("is_legendary") Boolean legendary)
    {
        this.name = name;
        this.flavorTextEntries = flavorTextEntries;
        this.habitat = habitat;
        this.legendary = legendary;
    }

    public String getName() {
        return name;
    }

    public List<PokemonFlavorTextEntryResponse> getFlavorTextEntries() {
        return flavorTextEntries;
    }

    public PokemonHabitatResponse getHabitat() {
        return habitat;
    }

    public Boolean getLegendary() {
        return legendary;
    }
}

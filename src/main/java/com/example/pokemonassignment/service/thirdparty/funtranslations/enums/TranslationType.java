package com.example.pokemonassignment.service.thirdparty.funtranslations.enums;

public enum TranslationType {
    YODA,
    SHAKESPEARE
}

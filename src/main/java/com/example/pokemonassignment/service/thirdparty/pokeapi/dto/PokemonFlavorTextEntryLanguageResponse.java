package com.example.pokemonassignment.service.thirdparty.pokeapi.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PokemonFlavorTextEntryLanguageResponse {
    private String name;

    @JsonCreator
    public PokemonFlavorTextEntryLanguageResponse(@JsonProperty("name") String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

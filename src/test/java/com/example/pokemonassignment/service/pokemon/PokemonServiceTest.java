package com.example.pokemonassignment.service.pokemon;

import com.example.pokemonassignment.exception.PokemonNotFoundException;
import com.example.pokemonassignment.model.Pokemon;
import com.example.pokemonassignment.model.PokemonBuilder;
import com.example.pokemonassignment.service.pokemon.converter.FunTranslationsResponseConverter;
import com.example.pokemonassignment.service.pokemon.converter.PokemonSpeciesResponseConverter;
import com.example.pokemonassignment.service.thirdparty.funtranslations.FunTranslationsService;
import com.example.pokemonassignment.service.thirdparty.funtranslations.dto.FunTranslationsResponse;
import com.example.pokemonassignment.service.thirdparty.funtranslations.dto.FunTranslationsResponseBuilder;
import com.example.pokemonassignment.service.thirdparty.funtranslations.enums.TranslationType;
import com.example.pokemonassignment.service.thirdparty.pokeapi.PokeApiService;
import com.example.pokemonassignment.service.thirdparty.pokeapi.dto.PokemonSpeciesResponse;
import com.example.pokemonassignment.service.thirdparty.pokeapi.dto.PokemonSpeciesResponseBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PokemonServiceTest {
    @Mock
    private PokeApiService pokeApiService;
    @Mock
    private FunTranslationsService funTranslationsService;
    @Mock
    private PokemonSpeciesResponseConverter pokemonSpeciesResponseConverter;
    @Mock
    private FunTranslationsResponseConverter funTranslationsResponseConverter;
    @InjectMocks
    private PokemonService sut;

    @AfterEach
    void afterEachTest() {
        verifyNoMoreInteractions(pokeApiService, funTranslationsService, pokemonSpeciesResponseConverter, funTranslationsResponseConverter);
    }

    @Test
    void shouldGetPokemonByName() {
        // Given
        String name = "name";

        PokemonSpeciesResponse pokemonSpeciesResponse = new PokemonSpeciesResponseBuilder().build();
        when(pokeApiService.getPokemonSpeciesByName(eq(name))).thenReturn(Optional.of(pokemonSpeciesResponse));

        Pokemon expected = new PokemonBuilder().build();
        when(pokemonSpeciesResponseConverter.convert(eq(pokemonSpeciesResponse))).thenReturn(expected);

        // When
        Pokemon actual = sut.getPokemonByName(name);

        // Then
        assertThat(actual).isEqualTo(expected);
        verify(pokeApiService).getPokemonSpeciesByName(eq(name));
        verify(pokemonSpeciesResponseConverter).convert(eq(pokemonSpeciesResponse));
    }

    @Test
    void shouldGetPokemonByName_pokemonNotFound() {
        // Given
        String name = "name";

        when(pokeApiService.getPokemonSpeciesByName(eq(name))).thenReturn(Optional.empty());

        // Then
        assertThatExceptionOfType(PokemonNotFoundException.class)
                .isThrownBy(() -> sut.getPokemonByName(name));
        verify(pokeApiService).getPokemonSpeciesByName(eq(name));
    }

    @Test
    void shouldGetPokemonTranslatedByName_isLegendary() {
        // Given
        String name = "name";

        PokemonSpeciesResponse pokemonSpeciesResponse = new PokemonSpeciesResponseBuilder().build();
        when(pokeApiService.getPokemonSpeciesByName(eq(name))).thenReturn(Optional.of(pokemonSpeciesResponse));

        Pokemon pokemon = new PokemonBuilder()
                .withName(name)
                .withDescription("Lorem ipsum")
                .withHabitat("non-cave")
                .withLegendary(true)
                .build();
        when(pokemonSpeciesResponseConverter.convert(eq(pokemonSpeciesResponse))).thenReturn(pokemon);

        FunTranslationsResponse funTranslationsResponse = new FunTranslationsResponseBuilder().build();
        when(funTranslationsService.getTranslation(eq(pokemon.getDescription()), eq(TranslationType.YODA)))
                .thenReturn(Optional.of(funTranslationsResponse));

        String expectedDescription = "Dolor sit amet";
        when(funTranslationsResponseConverter.convert(eq(funTranslationsResponse))).thenReturn(expectedDescription);

        // When
        Pokemon actual = sut.getPokemonTranslatedByName(name);

        // Then
        assertThat(actual)
                .matches(a -> a.getDescription().equals(expectedDescription))
                .usingRecursiveComparison()
                .ignoringFields("description")
                .isEqualTo(pokemon);
        verify(pokeApiService).getPokemonSpeciesByName(eq(name));
        verify(pokemonSpeciesResponseConverter).convert(eq(pokemonSpeciesResponse));
        verify(funTranslationsService).getTranslation(eq(pokemon.getDescription()), eq(TranslationType.YODA));
        verify(funTranslationsResponseConverter).convert(eq(funTranslationsResponse));
    }

    @Test
    void shouldGetPokemonTranslatedByName_habitatIsCave() {
        // Given
        String name = "name";

        PokemonSpeciesResponse pokemonSpeciesResponse = new PokemonSpeciesResponseBuilder().build();
        when(pokeApiService.getPokemonSpeciesByName(eq(name))).thenReturn(Optional.of(pokemonSpeciesResponse));

        Pokemon pokemon = new PokemonBuilder()
                .withName(name)
                .withDescription("Lorem ipsum")
                .withHabitat("cave")
                .withLegendary(false)
                .build();
        when(pokemonSpeciesResponseConverter.convert(eq(pokemonSpeciesResponse))).thenReturn(pokemon);

        FunTranslationsResponse funTranslationsResponse = new FunTranslationsResponseBuilder().build();
        when(funTranslationsService.getTranslation(eq(pokemon.getDescription()), eq(TranslationType.YODA)))
                .thenReturn(Optional.of(funTranslationsResponse));

        String expectedDescription = "Dolor sit amet";
        when(funTranslationsResponseConverter.convert(eq(funTranslationsResponse))).thenReturn(expectedDescription);

        // When
        Pokemon actual = sut.getPokemonTranslatedByName(name);

        // Then
        assertThat(actual)
                .matches(a -> a.getDescription().equals(expectedDescription))
                .usingRecursiveComparison()
                .ignoringFields("description")
                .isEqualTo(pokemon);
        verify(pokeApiService).getPokemonSpeciesByName(eq(name));
        verify(pokemonSpeciesResponseConverter).convert(eq(pokemonSpeciesResponse));
        verify(funTranslationsService).getTranslation(eq(pokemon.getDescription()), eq(TranslationType.YODA));
        verify(funTranslationsResponseConverter).convert(eq(funTranslationsResponse));
    }

    @Test
    void shouldGetPokemonTranslatedByName_neitherIsLegendaryNorHabitatIsCave() {
        // Given
        String name = "name";

        PokemonSpeciesResponse pokemonSpeciesResponse = new PokemonSpeciesResponseBuilder().build();
        when(pokeApiService.getPokemonSpeciesByName(eq(name))).thenReturn(Optional.of(pokemonSpeciesResponse));

        Pokemon pokemon = new PokemonBuilder()
                .withName(name)
                .withDescription("Lorem ipsum")
                .withHabitat("non-cave")
                .withLegendary(false)
                .build();
        when(pokemonSpeciesResponseConverter.convert(eq(pokemonSpeciesResponse))).thenReturn(pokemon);

        FunTranslationsResponse funTranslationsResponse = new FunTranslationsResponseBuilder().build();
        when(funTranslationsService.getTranslation(eq(pokemon.getDescription()), eq(TranslationType.SHAKESPEARE)))
                .thenReturn(Optional.of(funTranslationsResponse));

        String expectedDescription = "Dolor sit amet";
        when(funTranslationsResponseConverter.convert(eq(funTranslationsResponse))).thenReturn(expectedDescription);

        // When
        Pokemon actual = sut.getPokemonTranslatedByName(name);

        // Then
        assertThat(actual)
                .matches(a -> a.getDescription().equals(expectedDescription))
                .usingRecursiveComparison()
                .ignoringFields("description")
                .isEqualTo(pokemon);
        verify(pokeApiService).getPokemonSpeciesByName(eq(name));
        verify(pokemonSpeciesResponseConverter).convert(eq(pokemonSpeciesResponse));
        verify(funTranslationsService).getTranslation(eq(pokemon.getDescription()), eq(TranslationType.SHAKESPEARE));
        verify(funTranslationsResponseConverter).convert(eq(funTranslationsResponse));
    }

    @Test
    void shouldGetPokemonTranslatedByName_noYodaTranslation() {
        // Given
        String name = "name";

        PokemonSpeciesResponse pokemonSpeciesResponse = new PokemonSpeciesResponseBuilder().build();
        when(pokeApiService.getPokemonSpeciesByName(eq(name))).thenReturn(Optional.of(pokemonSpeciesResponse));

        Pokemon pokemon = new PokemonBuilder()
                .withName(name)
                .withDescription("Lorem ipsum")
                .withHabitat("cave")
                .withLegendary(true)
                .build();
        when(pokemonSpeciesResponseConverter.convert(eq(pokemonSpeciesResponse))).thenReturn(pokemon);

        when(funTranslationsService.getTranslation(eq(pokemon.getDescription()), eq(TranslationType.YODA)))
                .thenReturn(Optional.empty());

        // When
        Pokemon actual = sut.getPokemonTranslatedByName(name);

        // Then
        assertThat(actual)
                .usingRecursiveComparison()
                .isEqualTo(pokemon);
        verify(pokeApiService).getPokemonSpeciesByName(eq(name));
        verify(pokemonSpeciesResponseConverter).convert(eq(pokemonSpeciesResponse));
        verify(funTranslationsService).getTranslation(eq(pokemon.getDescription()), eq(TranslationType.YODA));
    }

    @Test
    void shouldGetPokemonTranslatedByName_noShakespeareTranslation() {
        // Given
        String name = "name";

        PokemonSpeciesResponse pokemonSpeciesResponse = new PokemonSpeciesResponseBuilder().build();
        when(pokeApiService.getPokemonSpeciesByName(eq(name))).thenReturn(Optional.of(pokemonSpeciesResponse));

        Pokemon pokemon = new PokemonBuilder()
                .withName(name)
                .withDescription("Lorem ipsum")
                .withHabitat("non-cave")
                .withLegendary(false)
                .build();
        when(pokemonSpeciesResponseConverter.convert(eq(pokemonSpeciesResponse))).thenReturn(pokemon);

        when(funTranslationsService.getTranslation(eq(pokemon.getDescription()), eq(TranslationType.SHAKESPEARE)))
                .thenReturn(Optional.empty());

        // When
        Pokemon actual = sut.getPokemonTranslatedByName(name);

        // Then
        assertThat(actual)
                .usingRecursiveComparison()
                .isEqualTo(pokemon);
        verify(pokeApiService).getPokemonSpeciesByName(eq(name));
        verify(pokemonSpeciesResponseConverter).convert(eq(pokemonSpeciesResponse));
        verify(funTranslationsService).getTranslation(eq(pokemon.getDescription()), eq(TranslationType.SHAKESPEARE));
    }

    @Test
    void shouldGetPokemonTranslatedByName_pokemonNotFound() {
        // Given
        String name = "name";

        when(pokeApiService.getPokemonSpeciesByName(eq(name))).thenReturn(Optional.empty());

        // Then
        assertThatExceptionOfType(PokemonNotFoundException.class)
                .isThrownBy(() -> sut.getPokemonTranslatedByName(name));
        verify(pokeApiService).getPokemonSpeciesByName(eq(name));
    }
}
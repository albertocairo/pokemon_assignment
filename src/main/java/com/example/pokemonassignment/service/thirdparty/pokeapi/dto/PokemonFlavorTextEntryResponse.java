package com.example.pokemonassignment.service.thirdparty.pokeapi.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PokemonFlavorTextEntryResponse {
    private String flavorText;
    private PokemonFlavorTextEntryLanguageResponse language;

    @JsonCreator
    public PokemonFlavorTextEntryResponse(
            @JsonProperty("flavor_text") String flavorText,
            @JsonProperty("language") PokemonFlavorTextEntryLanguageResponse language)
    {
        this.flavorText = flavorText;
        this.language = language;
    }

    public String getFlavorText() {
        return flavorText;
    }

    public PokemonFlavorTextEntryLanguageResponse getLanguage() {
        return language;
    }
}

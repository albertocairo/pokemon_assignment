package com.example.pokemonassignment.service.pokemon.converter;

import com.example.pokemonassignment.model.Pokemon;
import com.example.pokemonassignment.model.PokemonBuilder;
import com.example.pokemonassignment.service.thirdparty.pokeapi.dto.PokemonFlavorTextEntryResponse;
import com.example.pokemonassignment.service.thirdparty.pokeapi.dto.PokemonHabitatResponse;
import com.example.pokemonassignment.service.thirdparty.pokeapi.dto.PokemonSpeciesResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

@Component
public class PokemonSpeciesResponseConverter implements Converter<PokemonSpeciesResponse, Pokemon> {
    @Override
    public Pokemon convert(PokemonSpeciesResponse source) {
        Objects.requireNonNull(source);

        return new PokemonBuilder()
                .withName(source.getName())
                .withDescription(
                        Optional.ofNullable(source.getFlavorTextEntries())
                                .orElseGet(Collections::emptyList)
                                .stream()
                                .filter(entry -> "en".equalsIgnoreCase(entry.getLanguage().getName()))
                                .findFirst()
                                .map(PokemonFlavorTextEntryResponse::getFlavorText)
                                .orElse(null))
                .withHabitat(
                        Optional.ofNullable(source.getHabitat())
                                .map(PokemonHabitatResponse::getName)
                                .orElse(null))
                .withLegendary(source.getLegendary())
                .build();
    }
}

package com.example.pokemonassignment.service.thirdparty.pokeapi;

import com.example.pokemonassignment.service.thirdparty.pokeapi.dto.PokemonSpeciesResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Optional;

@Service
public class PokeApiService {
    private RestTemplate restTemplate;

    public PokeApiService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Optional<PokemonSpeciesResponse> getPokemonSpeciesByName(String name) {
        ResponseEntity<PokemonSpeciesResponse> response;
        try {
            response = restTemplate.exchange(
                    "https://pokeapi.co/api/v2/pokemon-species/{name}",
                    HttpMethod.GET,
                    null,
                    PokemonSpeciesResponse.class,
                    Collections.singletonMap("name", name));
        } catch (Exception e) {
            return Optional.empty();
        }

        return Optional.of(response)
                .map(ResponseEntity::getStatusCode)
                .filter(HttpStatus.OK::equals)
                .map(__ -> response.getBody());
    }
}

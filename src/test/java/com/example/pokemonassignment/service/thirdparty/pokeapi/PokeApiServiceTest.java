package com.example.pokemonassignment.service.thirdparty.pokeapi;

import com.example.pokemonassignment.service.thirdparty.pokeapi.dto.PokemonSpeciesResponse;
import com.example.pokemonassignment.service.thirdparty.pokeapi.dto.PokemonSpeciesResponseBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PokeApiServiceTest {
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private PokeApiService sut;

    @AfterEach()
    void afterEachTest() {
        verifyNoMoreInteractions(restTemplate);
    }

    @Test
    void shouldGetPokemonSpecies() {
        // Given
        String name = "name";
        PokemonSpeciesResponse expected = new PokemonSpeciesResponseBuilder().build();
        ResponseEntity<PokemonSpeciesResponse> response = ResponseEntity
                .ok()
                .body(expected);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), isNull(), eq(PokemonSpeciesResponse.class), anyMap()))
                .thenReturn(response);

        // When
        Optional<PokemonSpeciesResponse> actual = sut.getPokemonSpeciesByName(name);

        // Then
        assertThat(actual)
                .isPresent()
                .get()
                .isEqualTo(expected);
        verify(restTemplate).exchange(
                anyString(),
                eq(HttpMethod.GET),
                isNull(),
                eq(PokemonSpeciesResponse.class),
                refEq(Collections.singletonMap("name", name)));
    }

    @Test
    void shouldGetPokemonSpecies_exchangeThrowsException() {
        // Given
        String name = "name";
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), isNull(), eq(PokemonSpeciesResponse.class), anyMap()))
                .thenThrow(RuntimeException.class);

        // When
        Optional<PokemonSpeciesResponse> actual = sut.getPokemonSpeciesByName(name);

        // Then
        assertThat(actual).isEmpty();
        verify(restTemplate).exchange(
                anyString(),
                eq(HttpMethod.GET),
                isNull(),
                eq(PokemonSpeciesResponse.class),
                refEq(Collections.singletonMap("name", name)));
    }

    @Test
    void shouldGetPokemonSpecies_responseStatusCodeIsNotOk() {
        // Given
        String name = "name";
        ResponseEntity<PokemonSpeciesResponse> response = ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .build();
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), isNull(), eq(PokemonSpeciesResponse.class), anyMap()))
                .thenReturn(response);

        // When
        Optional<PokemonSpeciesResponse> actual = sut.getPokemonSpeciesByName(name);

        // Then
        assertThat(actual).isEmpty();
        verify(restTemplate).exchange(
                anyString(),
                eq(HttpMethod.GET),
                isNull(),
                eq(PokemonSpeciesResponse.class),
                refEq(Collections.singletonMap("name", name)));
    }
}